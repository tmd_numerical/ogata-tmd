#!/usr/bin/env python2

import numpy as np
from scipy.special import jv
from AdOg import AdOg

test = lambda b: b*np.exp(-b)

N=10
Q=1.0 # inverse of where test(b) peaks
q=1.0
nu=0

adog = AdOg(nu)

print 'adogt returns', adog.adogt(test,N,Q,nu,q)
print 'exact returns', 1/np.sqrt(8)
